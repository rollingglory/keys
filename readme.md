# Hello VM Users
If you happen to be needing an access to VM.

* Submit your public key merge request here please.
Please use your Rolling Glory mail username.
Example:
Your email is `newintern@rollingglory.com`
Then submit merge request for `newintern.pub` file filled with your pubkey.
I expect the public key to be in `ed25519` format.  
Preffered keypair creation: `ssh-keygen -t ed25519 -a 64`
* Ask machine Op to add yours.

# Hello Op
This repository is directed to sysadmin/devops/tech who create machine.

On Azure you may experience inability to use most of these key as root key.
Please, do use your own `rsa` public key.

After machine creation, it's machine creator responsibility to add users.

To add all users with key listed here:
```
sudo ./keys/addall.sh
```
That's it.

* You may add users manually using the `add.sh`.
```
sudo ./keys/add.sh bentinata
```
* Fuck up with the `addall.sh` somehow?
Remove all users except yourself.
```
sudo ./keys/removeall.sh
```

* And you may add `cronjob`, or webhook to periodically update.

* You're also free to modify the script;
although non-merged modification is not my resposibility.
